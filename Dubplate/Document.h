//
//  Document.h
//  Dubplate
//
//  Created by Andrew on 25/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class Pin, Metadata;

@interface Document : NSPersistentDocument<NSToolbarDelegate>

@property (nonatomic, assign) int playHead;
@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, strong) NSSet *selectedPins;
@property (nonatomic, strong) NSSet *clipboard;



-(void)togglePinAtX:(int)x y:(int)y command:(BOOL)commandHeldDown;
-(NSSet *)allPins;
-(Metadata *)metadata;
-(void)selectPinsFromTime:(int)fromTime toTime:(int)toTime fromNote:(int)fromNote toNote:(int)toNote;
-(IBAction)selectNone:(id)sender;
-(Pin *)pinAtX:(int)x y:(int)y;

-(IBAction)doubleNoteSpacing:(id)sender;
-(IBAction)halveNoteSpacing:(id)sender;

@end
