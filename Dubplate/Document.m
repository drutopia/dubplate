//
//  Document.m
//  Dubplate
//
//  Created by Andrew on 25/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import "Document.h"
#import "Pin.h"
#import "Metadata.h"
#import "MidiEngine.h"
#import "GridView.h"

@interface Document ()

@property (nonatomic, strong) Metadata *metadata;
@property (weak) IBOutlet NSSegmentedControl *gridLinesControl;
@property (weak) IBOutlet GridView *gridView;
@property (weak) IBOutlet NSToolbarItem *playPauseToolbarItem;
@property (nonatomic, strong) NSTimer *playbackTimer;

@end

@implementation Document



- (instancetype)init {
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
        NSLog(@"Started up");
        self.playHead = 0;
        self.isPlaying = 0;
    }
    return self;
}


- (void)windowControllerDidLoadNib:(NSWindowController *)aController {
    [super windowControllerDidLoadNib:aController];

    // set the minimum resize to something sensible
    NSWindowController *controller = [self.windowControllers firstObject];
    [controller.window setContentMinSize:NSMakeSize(400, 300)];
}

+ (BOOL)autosavesInPlace {
    return YES;
}

- (NSString *)windowNibName {
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"Document";
}

-(Pin *)pinAtX:(int)x y:(int)y {
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Pin"];
    NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"time = %d AND note = %d", x, y];
    [fetch setPredicate:filterPredicate];
    NSError *error;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    if (error) {
        NSLog(@"Error in togglePinAtX: %@", error);
    }
    Pin *pin = ([results firstObject]);
    return pin;
}

-(void)togglePinAtX:(int)x y:(int)y command:(BOOL)commandHeldDown {
    Pin *pin = [self pinAtX:x y:y];

    if (pin) {
        if (commandHeldDown && self.selectedPins) {
            self.selectedPins = [self.selectedPins setByAddingObject:pin];
        } else {
            self.selectedPins = [NSSet setWithObject:pin];
        }
//        [[self managedObjectContext] deleteObject:pin];
    } else if (self.selectedPins) {
        self.selectedPins = nil;
    } else {
        pin = [NSEntityDescription insertNewObjectForEntityForName:@"Pin" inManagedObjectContext:[self managedObjectContext]];
        pin.time = x;
        pin.note = y;
        [[MidiEngine instance] playNoteOnTrack:y];
    }
}

-(NSSet *)allPins {
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Pin"];
//    [fetch setSortDescriptors:@[
//                                [NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES],
//                                [NSSortDescriptor sortDescriptorWithKey:@"note" ascending:YES]]];
    NSError *error;
    NSArray *results = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
    if (error) {
        NSLog(@"Error in allPins: %@", error);
    }
    return [NSSet setWithArray:results];
}

-(NSArray *)pinsAtTime:(int)time {
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Pin"];
    [fetch setPredicate:[NSPredicate predicateWithFormat:@"time = %d", time]];
    NSArray *results = [[self managedObjectContext] executeFetchRequest:fetch error:nil];
    return results;
}

-(void)selectPinsFromTime:(int)fromTime toTime:(int)toTime fromNote:(int)fromNote toNote:(int)toNote {
    NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Pin"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"time >= %d AND time < %d AND note >= %d AND note < %d",
                              fromTime, toTime, fromNote, toNote];
    [fetch setPredicate:predicate];
    NSArray *results = [[self managedObjectContext] executeFetchRequest:fetch error:nil];
    self.selectedPins = [NSSet setWithArray:results];
}

-(void)copy:(id)sender {
    self.clipboard = [self.selectedPins copy];
}

-(void)cut:(id)sender {
    self.clipboard = [self.selectedPins copy];
    [self delete:sender];
}

-(void)paste:(id)sender {
    if (self.clipboard) {
        NSManagedObjectContext *context = [self managedObjectContext];
        int minTime = [[self.clipboard valueForKeyPath:@"@min.time"] intValue];
        int deltaTime = self.playHead - minTime;
        
        NSMutableSet *set = [NSMutableSet set];
        for (Pin *pin in self.clipboard) {
            Pin *newPin = [[Pin alloc] initWithEntity:pin.entity insertIntoManagedObjectContext:nil];
            newPin.note = pin.note;
            newPin.time = pin.time + deltaTime;
            [context insertObject:newPin];
            [set addObject:newPin];
        }
        self.selectedPins = [NSSet setWithSet:set];

        [self.gridView setNeedsDisplay:YES];
    }
}

-(void)selectAll:(id)sender {
    self.selectedPins = [self allPins];
    [self.gridView setNeedsDisplay:YES];
}

-(void)delete:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    for (Pin *pin in self.selectedPins) {
        [context deleteObject:pin];
    }
    self.selectedPins = nil;
    [self.gridView setNeedsDisplay:YES];
}

-(IBAction)selectNone:(id)sender {
    self.selectedPins = nil;
    [self.gridView setNeedsDisplay:YES];
}


-(Metadata *)metadata {
    if (!_metadata) {
        NSFetchRequest *fetch = [NSFetchRequest fetchRequestWithEntityName:@"Metadata"];
        NSError *error;
        NSArray *results = [[self managedObjectContext] executeFetchRequest:fetch error:&error];
        if (error) {
            NSLog(@"Error in metadata: %@", error);
        }
        if ([results count]) {
            _metadata = ([results firstObject]);
        } else {
            _metadata = [NSEntityDescription insertNewObjectForEntityForName:@"Metadata" inManagedObjectContext:[self managedObjectContext]];
        }
        [self.gridLinesControl selectSegmentWithTag:_metadata.timeGrid];
    }
    return _metadata;
}

- (IBAction)playOrPause:(id)sender {
    if (self.isPlaying) {
        self.isPlaying = NO;
        [self.playPauseToolbarItem setImage:[NSImage imageNamed:@"play"]];
        [self.playPauseToolbarItem setLabel:@"Play"];
    } else {
        self.isPlaying = YES;
        self.playbackTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(playbackTimerTick:) userInfo:nil repeats:YES];
        [self.playPauseToolbarItem setImage:[NSImage imageNamed:@"pause"]];
        [self.playPauseToolbarItem setLabel:@"Pause"];
    }
}
- (IBAction)resetToStart:(id)sender {
    self.playHead = 0;
    [self.gridView setNeedsDisplay:YES];
}
- (IBAction)rewind:(id)sender {
    self.playHead = MAX(self.playHead - self.metadata.timeGrid, 0);
    [self.gridView setNeedsDisplay:YES];
}
- (IBAction)forward:(id)sender {
    self.playHead = self.playHead + self.metadata.timeGrid;
    [self.gridView setNeedsDisplay:YES];
}

-(void)playbackTimerTick:(NSTimer *)sender {
    if (self.isPlaying) {
        // play notes and advance head
        NSArray *pins = [self pinsAtTime:self.playHead];
        for (Pin *nextPin in pins) {
            [[MidiEngine instance] playNoteOnTrack:nextPin.note];
        }
        [self.gridView setNeedsDisplay:YES];
        self.playHead++;
    } else {
        [self.playbackTimer invalidate];
        self.playbackTimer = nil;
    }
}


- (IBAction)segmentWasClicked:(id)sender {
    NSInteger grid;// = self.gridLinesControl.selectedTag; //that doesn't work for some reason
    switch (self.gridLinesControl.selectedSegment) {
        case 0:
            grid = 3;
            break;
        case 1:
            grid = 4;
            break;
        default:
            grid = 6;
    }
    NSLog(@"segmentWasClicked: %ld", grid);
    self.metadata.timeGrid = grid;
    [self.gridView setNeedsDisplay:YES];
}

-(IBAction)doubleNoteSpacing:(id)sender {
    NSLog(@"doubleNoteSpacing");
    NSSet *pins = (self.selectedPins ? self.selectedPins : [self allPins]);
    int minTime = [[pins valueForKeyPath:@"@min.time"] intValue];
    for (Pin *pin in pins) {
        pin.time = (pin.time - minTime) * 2;
    }
    
    [self.gridView setNeedsDisplay:YES];
}
-(IBAction)halveNoteSpacing:(id)sender {
    NSLog(@"halveNoteSpacing");    
    NSSet *pins = (self.selectedPins ? self.selectedPins : [self allPins]);
    int minTime = [[pins valueForKeyPath:@"@min.time"] intValue];
    for (Pin *pin in pins) {
        pin.time = (pin.time - minTime) / 2;
    }
    [self.gridView setNeedsDisplay:YES];
}

-(IBAction)export:(id)sender {
}

@end
