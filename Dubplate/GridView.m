//
//  GridView.m
//  Dubplate
//
//  Created by Andrew on 25/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import "GridView.h"
#import "Document.h"
#import "Pin.h"
#import "Metadata.h"

static const CGFloat DASH[] = {3.0, 3.0};

@interface GridView ()

@property (nonatomic, assign) CGFloat cellSize;
@property (nonatomic, assign) int gridInterval;
@property (nonatomic, assign) NSPoint lastPoint;
@property (nonatomic, assign) NSRect selectionRect;


@end

@implementation GridView

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSLog(@"Init");
        [self setAcceptsTouchEvents:YES];
        self.gridInterval = 4;
        self.selectionRect = NSZeroRect;
    }
    return self;
}

-(BOOL)gridIndexIsClickable:(int)gridIndex {
    static BOOL clickable[] = {YES, NO, NO, YES, YES, YES, NO, YES, YES, YES,
        YES, YES, YES, YES, YES, YES, YES, YES, YES};
    if (gridIndex < 0 || gridIndex >= 19) {
        return NO;
    } else {
        return clickable[gridIndex];
    }
}

// notes: G, A, B, C - B, C- B, C, D = 19

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    NSSize size = self.frame.size;
    NSLog(@"Size = %@", NSStringFromSize(size));

    self.cellSize = size.height / 19;

    [[NSColor lightGrayColor] setStroke];
    NSBezierPath *gridPath = [[NSBezierPath alloc] init];
    [[NSColor grayColor] setFill];
    for (int i = 0; i < 19; i++) {
        CGFloat y = _cellSize * i;
        if (i > 0) {
            [gridPath moveToPoint:NSMakePoint(0, y)];
            [gridPath lineToPoint:NSMakePoint(size.width, y)];
        }
        if (![self gridIndexIsClickable:i]) {
            NSRect disabledRect = NSMakeRect(0, y, size.width, _cellSize);
            NSRectFill(disabledRect);
        }
    }
    [gridPath stroke];

    Document *doc = [[[self window] windowController] document];
    _gridInterval = doc.metadata.timeGrid;
    
    int gridIndex = 0;
    int docPlayhead = doc.playHead;
    for (CGFloat x = 0;x < size.width; x+= _cellSize) {
        @autoreleasepool {
            if (gridIndex == docPlayhead) {
                [[NSColor redColor] setStroke];
            } else if (gridIndex == 0) {
                [[NSColor clearColor] setStroke];
            } else if (gridIndex == docPlayhead + 1 || gridIndex == 1) {
                [[NSColor lightGrayColor] setStroke];
            }
            NSBezierPath *gridColumn = [[NSBezierPath alloc] init];
            [gridColumn moveToPoint:NSMakePoint(x, 0)];
            [gridColumn lineToPoint:NSMakePoint(x, size.height)];
            [gridColumn setLineWidth:(gridIndex % _gridInterval == 0 ? 3.0 : 1.0)];
            [gridColumn stroke];
        }
        gridIndex++;
    }
    // now the pins
    NSSet *pins = [doc allPins];
    NSSet *selectedPins = doc.selectedPins;
    [[NSColor purpleColor] setFill];
    CGFloat inset = _cellSize / 4;
    for (Pin *pin in pins) {
        @autoreleasepool {
            NSRect circleRect = NSMakeRect(_cellSize * pin.time, _cellSize * pin.note, _cellSize, _cellSize);
            circleRect = NSInsetRect(circleRect, inset, inset);
            if (selectedPins) {
                [([selectedPins containsObject:pin] ? [NSColor redColor] : [NSColor purpleColor]) setFill];
            }
            [[NSBezierPath bezierPathWithOvalInRect:circleRect] fill];
        }
    }
    if (!(self.selectionRect.size.height == 0 && self.selectionRect.size.width == 0)) {
        NSBezierPath *selectionPath = [NSBezierPath bezierPathWithRect:self.selectionRect];
        [[NSColor darkGrayColor] setStroke];
        [selectionPath setLineDash:DASH count:2 phase:0.5];
        [selectionPath stroke];

    }
}

-(void)mouseDown:(NSEvent *)theEvent {
    NSPoint locInWindow;
    locInWindow = [theEvent locationInWindow];
    NSPoint lastPoint = [self convertPoint:locInWindow fromView:nil];
    
    self.lastPoint = lastPoint;
    
//    Document *doc = [[[self window] windowController] document];
    
}

-(void)mouseDragged:(NSEvent *)theEvent {
    NSPoint locInWindow;
    NSPoint locInView;
    locInWindow = [theEvent locationInWindow];
    locInView = [self convertPoint:locInWindow fromView:nil];

    CGFloat x1 = MIN(locInView.x, self.lastPoint.x);
    CGFloat x2 = MAX(locInView.x, self.lastPoint.x);
    CGFloat y1 = MIN(locInView.y, self.lastPoint.y);
    CGFloat y2 = MAX(locInView.y, self.lastPoint.y);

    self.selectionRect = NSMakeRect(x1, y1, x2 - x1, y2 - y1);
    [self setNeedsDisplay:YES];
}

-(void)mouseUp:(NSEvent *)event {
    Document *doc = [[[self window] windowController] document];
    if (self.selectionRect.size.height == 0 && self.selectionRect.size.width == 0) {
//        if (doc.selectedPins) {
//            [doc selectNone:event];
//        } else {
            NSPoint locInWindow;
            NSPoint locInView;
            
            locInWindow = [event locationInWindow];
            locInView = [self convertPoint:locInWindow fromView:nil];
            
        //    NSLog(@"Location in window: %@", NSStringFromPoint(locInWindow));
        //    NSLog(@"Location in view: %@", NSStringFromPoint(locInView));
        //    
            int gridX = floor(locInView.x / _cellSize);
            int gridY = floor(locInView.y / _cellSize);
            
            if ([self gridIndexIsClickable:gridY]) {
                NSLog(@"CLICK! %d, %d", gridX, gridY);
                [self toggleNote:gridY atLocation:gridX modifiers:event.modifierFlags];
            }
//        }
    } else {

        // capture selection
        int fromTime = round(self.selectionRect.origin.x / _cellSize);
        int toTime = round((self.selectionRect.origin.x + self.selectionRect.size.width) / _cellSize);
        int fromNote = round(self.selectionRect.origin.y / _cellSize);
        int toNote = round((self.selectionRect.origin.y + self.selectionRect.size.height) / _cellSize);
        [doc selectPinsFromTime:fromTime toTime:toTime fromNote:fromNote toNote:toNote];
        
        self.selectionRect = NSZeroRect;

    }
    [self setNeedsDisplay:YES];
}

-(void)toggleNote:(int)y atLocation:(int)x modifiers:(NSUInteger)modifiers {
    Document *doc = [[[self window] windowController] document];
    NSLog(@"mods %lx contain %x? %lu", modifiers, NSCommandKeyMask, (modifiers & NSCommandKeyMask));
    BOOL commandHeld = (modifiers & NSCommandKeyMask) != 0;
    [doc togglePinAtX:x y:y command:commandHeld];
//    [self setNeedsDisplay:YES];
}

@end
