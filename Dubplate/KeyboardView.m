//
//  KeyboardView.m
//  Dubplate
//
//  Created by Andrew on 25/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import "KeyboardView.h"

@implementation KeyboardView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    BOOL blackKeys[] = {YES, YES, YES, NO, YES, YES, NO, YES, YES, YES, NO, YES, YES, NO, YES, YES, YES, NO, YES, YES};
    
    NSSize size = self.frame.size;
    NSLog(@"Size = %@", NSStringFromSize(size));
    
    CGFloat cellSize = size.height / 19;
    
    [[NSColor whiteColor] setFill];

    NSRectFill(dirtyRect);
    
    //    NSGraphicsContext *ctxt = [NSGraphicsContext currentContext];
    [[NSColor lightGrayColor] setStroke];
    
    
    NSBezierPath *gridPath = [[NSBezierPath alloc] init];
    
    for (int i = 1; i < 19; i++) {
        CGFloat y = cellSize * i;
        [gridPath moveToPoint:NSMakePoint(0, y)];
        [gridPath lineToPoint:NSMakePoint(size.width, y)];
    }
    [gridPath stroke];

    [[NSColor darkGrayColor] setFill];
    
    NSRect blackNoteRect = NSMakeRect(0, 0, 66, cellSize * 2 / 3);
    for (int i = 0; i <= 19; i++) {
        CGFloat y = cellSize * i;
        if (blackKeys[i]) {
            blackNoteRect.origin.y = y - (cellSize / 3);
            NSRectFill(blackNoteRect);
        }
    }

    NSDictionary *attrs = @{
                            NSForegroundColorAttributeName: [NSColor lightGrayColor],
                            NSFontAttributeName: [NSFont systemFontOfSize:16]
                            };
    for (int i = 3; i <= 19; i += 7) {
        CGFloat y = cellSize * i;
        [@"C" drawAtPoint:NSMakePoint(80, y) withAttributes:attrs];
    }
    

}

@end
