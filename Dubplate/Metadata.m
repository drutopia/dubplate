//
//  Metadata.m
//  Dubplate
//
//  Created by Andrew on 26/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import "Metadata.h"


@implementation Metadata

@dynamic notes;
@dynamic duration;
@dynamic title;
@dynamic timeGrid;

@end
