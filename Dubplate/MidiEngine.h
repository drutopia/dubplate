//
//  MidiEngine.h
//  Dubplate
//
//  Created by Andrew on 26/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MidiEngine : NSObject

-(void)playNoteOnTrack:(int)trackIndex;
+(instancetype)instance;

@end
