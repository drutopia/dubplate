//
//  MidiEngine.m
//  Dubplate
//
//  Created by Andrew on 26/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import "MidiEngine.h"
#include <CoreServices/CoreServices.h> //for file stuff
#include <AudioUnit/AudioUnit.h>
#include <AudioToolbox/AudioToolbox.h> //for AUGraph

enum {
    kMidiMessage_ControlChange 		= 0xB,
    kMidiMessage_ProgramChange 		= 0xC,
    kMidiMessage_BankMSBControl 	= 0,
    kMidiMessage_BankLSBControl		= 32,
    kMidiMessage_NoteOn 			= 0x9
};

// This call creates the Graph and the Synth unit...
OSStatus	CreateAUGraph (AUGraph &outGraph, AudioUnit &outSynth)
{
    OSStatus result;
    //create the nodes of the graph
    AUNode synthNode, limiterNode, outNode;
    
    AudioComponentDescription cd;
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags = 0;
    cd.componentFlagsMask = 0;
    
    require_noerr (result = NewAUGraph (&outGraph), home);
    
    cd.componentType = kAudioUnitType_MusicDevice;
    cd.componentSubType = kAudioUnitSubType_DLSSynth;
    
    require_noerr (result = AUGraphAddNode (outGraph, &cd, &synthNode), home);
    
    cd.componentType = kAudioUnitType_Effect;
    cd.componentSubType = kAudioUnitSubType_PeakLimiter;
    
    require_noerr (result = AUGraphAddNode (outGraph, &cd, &limiterNode), home);
    
    cd.componentType = kAudioUnitType_Output;
    cd.componentSubType = kAudioUnitSubType_DefaultOutput;
    require_noerr (result = AUGraphAddNode (outGraph, &cd, &outNode), home);
    
    require_noerr (result = AUGraphOpen (outGraph), home);
    
    require_noerr (result = AUGraphConnectNodeInput (outGraph, synthNode, 0, limiterNode, 0), home);
    require_noerr (result = AUGraphConnectNodeInput (outGraph, limiterNode, 0, outNode, 0), home);
    
    // ok we're good to go - get the Synth Unit...
    require_noerr (result = AUGraphNodeInfo(outGraph, synthNode, 0, &outSynth), home);
    
home:
    return result;
}

void ReleaseMIDI(AUGraph graph) {
    if (graph) {
        AUGraphStop (graph); // stop playback - AUGraphDispose will do that for us but just showing you what to do
        DisposeAUGraph (graph);
    }
}

static const int MIDI_NOTE_COUNT = 19;
static const int MIDI_CHANNEL = 0; // channel 1
static const int MIDI_NOTE_LIST[] = {67, 0, 0, 72, 74, 76, 0, 79, 81, 83, 84, 86, 88, 89, 91, 93, 95, 96, 98};

@implementation MidiEngine {
    AUGraph _graph;
    AudioUnit _synthUnit;
}

+(instancetype)instance {
    static MidiEngine *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[MidiEngine alloc] init];
    });
    return _instance;
}

-(id)init {
    self = [super init];
    if (self) {
        _graph = [self initializeMIDI];
        if (!_graph) return nil;
    }
    return self;
}

-(AUGraph)initializeMIDI {
    AUGraph graph = 0;
    
    OSStatus result;
    
    UInt8 midiChannelInUse = MIDI_CHANNEL;
    
    require_noerr (result = CreateAUGraph (graph, _synthUnit), home);
    // ok we're set up to go - initialize and start the graph
    require_noerr (result = AUGraphInitialize (graph), home);
    
    //set our bank
    require_noerr (result = MusicDeviceMIDIEvent(_synthUnit,
                                                 kMidiMessage_ControlChange << 4 | midiChannelInUse,
                                                 kMidiMessage_BankMSBControl, 0,
                                                 0/*sample offset*/), home);
    
    require_noerr (result = MusicDeviceMIDIEvent(_synthUnit,
                                                 kMidiMessage_ProgramChange << 4 | midiChannelInUse,
                                                 8/*prog change num*/, 0, // celesta
                                                 0/*sample offset*/), home);
    
    require_noerr (result = AUGraphStart (graph), home);
    
    return graph;
    
home:
    ReleaseMIDI(graph);
    return 0;
    
}

-(void)playNoteOnTrack:(int)trackIndex {
    if (trackIndex >= 0 && trackIndex < MIDI_NOTE_COUNT) {
        OSStatus result = MusicDeviceMIDIEvent(_synthUnit, kMidiMessage_NoteOn << 4 | MIDI_CHANNEL, MIDI_NOTE_LIST[trackIndex], 127, 0);

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            OSStatus result = MusicDeviceMIDIEvent(_synthUnit, kMidiMessage_NoteOn << 4 | MIDI_CHANNEL, MIDI_NOTE_LIST[trackIndex], 0, 0);
            
        });
    }
}

-(void)dealloc {
    ReleaseMIDI(_graph);
    _graph = 0;
}


@end
