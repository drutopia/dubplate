//
//  Pin.h
//  Dubplate
//
//  Created by Andrew on 25/09/2014.
//  Copyright (c) 2014 Marmadore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Pin : NSManagedObject

@property (nonatomic) int16_t time;
@property (nonatomic) int16_t note;


@end
