# README #

Dubplate is a Mac OS X editor for the original Fisher Price® record player

This project was inspired by Fred Murphy's CNC milled/3D printed custom records (http://www.instructables.com/id/3D-printing-records-for-a-Fisher-Price-toy-record-/)

### Build requirements ###

The project builds in Xcode 6. All self-explanatory: no special dependencies.